package bg.tilchev.tauliainvoice;

import bg.tilchev.tauliainvoice.service.InvoiceDeserializer;
import bg.tilchev.tauliainvoice.service.InvoiceSerializer;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class TauliaInvoiceApplication {

    private final InvoiceSerializer invoiceXmlJacksonSerializer;
    private final InvoiceSerializer invoiceCsvJacksonSerializer;
    private final InvoiceSerializer invoiceCsvOpenSerializer;
    private final InvoiceDeserializer invoiceCsvJacksonDeserializer;
    private final InvoiceDeserializer invoiceCsvOpenDeserializer;

    public static void main(String[] args) {
        SpringApplication.run(TauliaInvoiceApplication.class, args);
    }
}
