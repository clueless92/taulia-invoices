package bg.tilchev.tauliainvoice.config;

import bg.tilchev.tauliainvoice.service.InvoiceDeserializer;
import bg.tilchev.tauliainvoice.service.impl.CsvJacksonInvoiceDeserializer;
import bg.tilchev.tauliainvoice.service.impl.CsvOpenInvoiceDeserializer;
import org.apache.commons.io.input.BOMInputStream;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.function.Supplier;

/**
 * Created on 2022-10-27.
 */
@Configuration
public class DeserializerConfiguration {

    @Value("read.path")
    private static String readPath;

    private static Supplier<Reader> getReaderSupplier() {
        return () -> {
            try {
                return new InputStreamReader(new BOMInputStream(new FileInputStream(readPath)));
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            }
        };
    }

    @Bean
    public InvoiceDeserializer invoiceCsvJacksonDeserializer() {
        return new CsvJacksonInvoiceDeserializer(getReaderSupplier());
    }

    @Bean
    public InvoiceDeserializer invoiceCsvOpenDeserializer() {
        return new CsvOpenInvoiceDeserializer(getReaderSupplier());
    }
}
