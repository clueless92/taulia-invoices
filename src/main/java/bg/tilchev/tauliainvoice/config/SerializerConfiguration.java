package bg.tilchev.tauliainvoice.config;

import bg.tilchev.tauliainvoice.service.InvoiceSerializer;
import bg.tilchev.tauliainvoice.service.impl.CsvJacksonInvoiceSerializer;
import bg.tilchev.tauliainvoice.service.impl.CsvOpenInvoiceSerializer;
import bg.tilchev.tauliainvoice.service.impl.XmlJacksonInvoiceSerializer;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * Created on 2022-10-27.
 */
@Configuration
public class SerializerConfiguration {

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public InvoiceSerializer invoiceCsvJacksonSerializer() {
        return new CsvJacksonInvoiceSerializer();
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public InvoiceSerializer invoiceXmlJacksonSerializer() {
        return new XmlJacksonInvoiceSerializer();
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public InvoiceSerializer invoiceCsvOpenSerializer() {
        return new CsvOpenInvoiceSerializer();
    }
}
