package bg.tilchev.tauliainvoice.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.opencsv.bean.CsvBindByPosition;
import com.opencsv.bean.CsvDate;
import lombok.Data;

import java.time.LocalDate;

/**
 * Created on 2022-10-20.
 */
@Data
@JacksonXmlRootElement(localName = "invoice")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Invoice {

    @JsonProperty("buyer")
    @CsvBindByPosition(position = 0, required = true)
    private String buyer;

    @JacksonXmlProperty(localName = "image_name")
    @JsonProperty("image_name")
    @CsvBindByPosition(position = 1)
    private String imageName;

    @JacksonXmlProperty(localName = "invoice_image")
    @JsonProperty("invoice_image")
    @CsvBindByPosition(position = 2)
    private String invoiceImage;

    @JacksonXmlProperty(localName = "invoice_due_date")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonProperty("invoice_due_date")
    @CsvBindByPosition(position = 3)
    @CsvDate("yyyy-MM-dd")
    private LocalDate invoiceDueDate;

    @JacksonXmlProperty(localName = "invoice_number")
    @JsonProperty("invoice_number")
    @CsvBindByPosition(position = 4)
    private String invoiceNumber;

    @JacksonXmlProperty(localName = "invoice_amount")
    @JsonProperty("invoice_amount")
    @CsvBindByPosition(position = 5)
    private Double invoiceAmount;

    @JacksonXmlProperty(localName = "invoice_currency")
    @JsonProperty("invoice_currency")
    @CsvBindByPosition(position = 6)
    private String invoiceCurrency;

    @JacksonXmlProperty(localName = "invoice_status")
    @JsonProperty("invoice_status")
    @CsvBindByPosition(position = 7)
    private String invoiceStatus;

    @CsvBindByPosition(position = 8)
    @JsonProperty("supplier")
    private String supplier;
}
