package bg.tilchev.tauliainvoice.service;

import bg.tilchev.tauliainvoice.model.Invoice;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.function.Consumer;

/**
 * Created on 2022-10-20.
 */
@Service
public interface InvoiceDeserializer {

    void deserialize(Consumer<Invoice> consumer) throws IOException;
}
