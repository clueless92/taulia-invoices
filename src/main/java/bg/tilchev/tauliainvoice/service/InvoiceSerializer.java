package bg.tilchev.tauliainvoice.service;

import bg.tilchev.tauliainvoice.model.Invoice;
import org.springframework.stereotype.Service;

import java.io.File;

/**
 * Created on 2022-10-20.
 */
@Service
public interface InvoiceSerializer {

    void serialize(Invoice invoice);

    void setWritePath(File path);
}
