package bg.tilchev.tauliainvoice.service.impl;

import bg.tilchev.tauliainvoice.service.InvoiceDeserializer;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.Reader;
import java.util.function.Supplier;

/**
 * Created on 2022-10-20.
 */
@Service
@RequiredArgsConstructor
public abstract class BaseInvoiceDeserializer implements InvoiceDeserializer {

    protected final Supplier<Reader> readerSupplier;
}
