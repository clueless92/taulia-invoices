package bg.tilchev.tauliainvoice.service.impl;

import bg.tilchev.tauliainvoice.service.InvoiceSerializer;
import lombok.Setter;
import org.springframework.stereotype.Service;

import java.io.File;

/**
 * Created on 2022-10-20.
 */
@Setter
@Service
public abstract class BaseInvoiceSerializer implements InvoiceSerializer {

    protected File writePath;
}
