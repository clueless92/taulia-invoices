package bg.tilchev.tauliainvoice.service.impl;

import bg.tilchev.tauliainvoice.model.Invoice;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.Reader;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Created on 2022-10-20.
 */
@Slf4j
public class CsvJacksonInvoiceDeserializer extends BaseInvoiceDeserializer {

    public CsvJacksonInvoiceDeserializer(Supplier<Reader> readerSupplier) {
        super(readerSupplier);
    }

    @Override
    public void deserialize(Consumer<Invoice> consumer) throws IOException {
        var mapper = CsvMapper.builder()
                .disable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY)
                .build();
        mapper.registerModule(new JavaTimeModule());
        var schema = mapper.schemaFor(Invoice.class).withoutQuoteChar().withHeader();
        try (MappingIterator<Invoice> it = mapper.readerFor(Invoice.class)
                .with(schema).readValues(this.readerSupplier.get())) {
            it.forEachRemaining(consumer);
        }
    }
}
