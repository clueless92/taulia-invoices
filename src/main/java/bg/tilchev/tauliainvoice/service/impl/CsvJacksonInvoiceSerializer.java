package bg.tilchev.tauliainvoice.service.impl;

import bg.tilchev.tauliainvoice.model.Invoice;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created on 2022-10-20.
 */
public class CsvJacksonInvoiceSerializer extends BaseInvoiceSerializer {

    @Override
    public void serialize(Invoice invoice) {
        this.writePath.mkdirs();
        var file = new File(this.writePath, invoice.getBuyer() + ".csv");
        var mapper = CsvMapper.builder()
                .disable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY)
                .build();
        mapper.registerModule(new JavaTimeModule());
        var schema = mapper.schemaFor(Invoice.class).withoutQuoteChar();
        if (!file.exists()) {
            schema = schema.withHeader();
        }
        try (var writer = new FileWriter(file, true)) {
            mapper.writerFor(Invoice.class).with(schema).writeValue(writer, invoice);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
