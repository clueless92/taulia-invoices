package bg.tilchev.tauliainvoice.service.impl;

import bg.tilchev.tauliainvoice.model.Invoice;
import com.opencsv.bean.CsvToBeanBuilder;
import lombok.extern.slf4j.Slf4j;

import java.io.Reader;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Created on 2022-10-20.
 */
@Slf4j
public class CsvOpenInvoiceDeserializer extends BaseInvoiceDeserializer {

    public CsvOpenInvoiceDeserializer(Supplier<Reader> readerSupplier) {
        super(readerSupplier);
    }

    @Override
    public void deserialize(Consumer<Invoice> consumer) {
        try (var stream = new CsvToBeanBuilder<Invoice>(this.readerSupplier.get())
                .withType(Invoice.class)
                .withSkipLines(1) // skip headers; read/write based on position, not to change column order
                .build()
                .stream()) {
            stream.forEach(consumer);
        }
    }
}
