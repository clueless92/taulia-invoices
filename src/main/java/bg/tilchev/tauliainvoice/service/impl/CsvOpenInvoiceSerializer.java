package bg.tilchev.tauliainvoice.service.impl;

import bg.tilchev.tauliainvoice.model.Invoice;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import org.apache.commons.lang3.reflect.FieldUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Created on 2022-10-20.
 */
public class CsvOpenInvoiceSerializer extends BaseInvoiceSerializer {

    @Override
    public void serialize(Invoice invoice) {
        this.writePath.mkdirs();
        var file = new File(this.writePath, invoice.getBuyer() + ".csv");
        boolean exists = file.exists();
        try (var writer = new FileWriter(file, true)) {
            if (!exists) {
                writeCsvHeaders(invoice, writer);
            }
            new StatefulBeanToCsvBuilder<Invoice>(writer)
                    .withApplyQuotesToAll(false)
                    .build()
                    .write(invoice);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static void writeCsvHeaders(Invoice invoice, FileWriter writer) throws IOException {
        String headers = Arrays.stream(FieldUtils.getAllFields(invoice.getClass())).map(f -> {
            var annotation = f.getAnnotation(JacksonXmlProperty.class);
            if (annotation == null) {
                return f.getName();
            }
            return annotation.localName();
        }).collect(Collectors.joining(","));
        writer.write(headers);
        writer.write(System.lineSeparator());
    }
}
