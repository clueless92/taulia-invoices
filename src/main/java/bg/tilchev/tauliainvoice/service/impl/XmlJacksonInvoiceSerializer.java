package bg.tilchev.tauliainvoice.service.impl;

import bg.tilchev.tauliainvoice.model.Invoice;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Base64;

/**
 * Created on 2022-10-20.
 */
public class XmlJacksonInvoiceSerializer extends BaseInvoiceSerializer {

    private static final String ROOT_TAG_TXT = Invoice.class.getSimpleName().toLowerCase();
    private static final String ROOT_START_TAG = String.format("<%1$ss>%n", ROOT_TAG_TXT);
    private static final String ROOT_END_TAG = String.format("</%1$ss>%n", ROOT_TAG_TXT);
    private static final String XML_HEADER = "<?xml version='1.0' encoding='UTF-8'?>" + System.lineSeparator();

    @Override
    public void serialize(Invoice invoice) {
        this.writePath.mkdirs();
        var file = new File(this.writePath, invoice.getBuyer() + ".xml");
        boolean exists = file.exists();
        this.decodeToImage(invoice);
        try (var writer = new RandomAccessFile(file, "rw")) {
            if (!exists) {
                writer.writeBytes(XML_HEADER);
                writer.writeBytes(ROOT_START_TAG);
            }
            String xml = XmlMapper.builder()
                    .build()
                    .configure(SerializationFeature.INDENT_OUTPUT, true)
                    .registerModule(new JavaTimeModule())
                    .writeValueAsString(invoice);
            if (exists) {
                writer.seek(writer.length() - ROOT_END_TAG.length());
            }
            writer.writeBytes(xml);
            writer.writeBytes(ROOT_END_TAG);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void decodeToImage(Invoice invoice) {
        if (StringUtils.isBlank(invoice.getInvoiceImage())) {
            return;
        }
        byte[] imageBytes = Base64.getDecoder().decode(invoice.getInvoiceImage());
        try (ByteArrayInputStream bis = new ByteArrayInputStream(imageBytes)) {
            BufferedImage image = ImageIO.read(bis);
            if (StringUtils.isBlank(invoice.getImageName())) {
                invoice.setImageName("image.png");
            }
            String imageExtension = invoice.getImageName()
                    .substring(invoice.getImageName().lastIndexOf(".") - 1);
            var imageInFS = new File(this.writePath, invoice.getImageName());
            if (imageInFS.createNewFile()) {
                invoice.setInvoiceImage(FilenameUtils.separatorsToUnix(imageInFS.getPath()));
                ImageIO.write(image, imageExtension, imageInFS);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
