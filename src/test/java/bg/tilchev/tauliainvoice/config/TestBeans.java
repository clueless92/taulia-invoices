package bg.tilchev.tauliainvoice.config;

import bg.tilchev.tauliainvoice.service.InvoiceDeserializer;
import bg.tilchev.tauliainvoice.service.InvoiceSerializer;
import bg.tilchev.tauliainvoice.service.impl.*;
import org.apache.commons.io.input.BOMInputStream;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.function.Supplier;

/**
 * Created on 2022-10-25.
 */
@TestConfiguration
public class TestBeans {

    private static final String READ_PATH = "src/test/resources/input/invoices.csv";

    private static Supplier<Reader> getReaderSupplier() {
        return () -> {
            try {
                return new InputStreamReader(new BOMInputStream(new FileInputStream(READ_PATH)));
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            }
        };
    }

    @Bean
    public InvoiceDeserializer invoiceCsvJacksonDeserializer() {
        return new CsvJacksonInvoiceDeserializer(getReaderSupplier());
    }

    @Bean
    public InvoiceDeserializer invoiceCsvOpenDeserializer() {
        return new CsvOpenInvoiceDeserializer(getReaderSupplier());
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public InvoiceSerializer invoiceCsvJacksonSerializer() {
        return new CsvJacksonInvoiceSerializer();
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public InvoiceSerializer invoiceXmlJacksonSerializer() {
        return new XmlJacksonInvoiceSerializer();
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public InvoiceSerializer invoiceCsvOpenSerializer() {
        return new CsvOpenInvoiceSerializer();
    }
}
