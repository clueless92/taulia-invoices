package bg.tilchev.tauliainvoice.integration;

import bg.tilchev.tauliainvoice.config.TestBeans;
import bg.tilchev.tauliainvoice.service.InvoiceDeserializer;
import bg.tilchev.tauliainvoice.service.InvoiceSerializer;
import bg.tilchev.tauliainvoice.util.FileUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.io.File;
import java.io.IOException;

/**
 * Created on 2022-10-20.
 */
@SpringBootTest(classes = TestBeans.class)
@ActiveProfiles("test")
public class JacksonCsvInvoicesTest {

    private static final File ACTUAL_RESULTS = new File("build/src/test/resources/output/"
            + JacksonCsvInvoicesTest.class.getSimpleName());
    private final InvoiceSerializer invoiceCsvJacksonSerializer;
    private final InvoiceDeserializer invoiceCsvJacksonDeserializer;

    @Autowired
    public JacksonCsvInvoicesTest(InvoiceSerializer invoiceCsvJacksonSerializer,
                                  InvoiceDeserializer invoiceCsvJacksonDeserializer) {
        this.invoiceCsvJacksonSerializer = invoiceCsvJacksonSerializer;
        this.invoiceCsvJacksonDeserializer = invoiceCsvJacksonDeserializer;
    }

    @BeforeEach
    public void setup() {
        FileUtils.deleteFilesInDir(ACTUAL_RESULTS);
        this.invoiceCsvJacksonSerializer.setWritePath(ACTUAL_RESULTS);
    }

    @Test
    public void testCsv() throws IOException {
        this.invoiceCsvJacksonDeserializer.deserialize(this.invoiceCsvJacksonSerializer::serialize);
        FileUtils.assertFilesEqual(ACTUAL_RESULTS, ".csv");
    }
}
