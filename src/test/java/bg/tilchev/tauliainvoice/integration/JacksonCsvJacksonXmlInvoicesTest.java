package bg.tilchev.tauliainvoice.integration;

import bg.tilchev.tauliainvoice.config.TestBeans;
import bg.tilchev.tauliainvoice.service.InvoiceDeserializer;
import bg.tilchev.tauliainvoice.service.InvoiceSerializer;
import bg.tilchev.tauliainvoice.util.FileUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.io.File;
import java.io.IOException;

/**
 * Created on 2022-10-20.
 */
@SpringBootTest(classes = TestBeans.class)
@ActiveProfiles("test")
public class JacksonCsvJacksonXmlInvoicesTest {

    private static final File ACTUAL_RESULTS = new File("build/src/test/resources/output/"
            + JacksonCsvJacksonXmlInvoicesTest.class.getSimpleName());

    private final InvoiceSerializer invoiceXmlJacksonSerializer;
    private final InvoiceDeserializer invoiceCsvJacksonDeserializer;

    @Autowired
    public JacksonCsvJacksonXmlInvoicesTest(InvoiceSerializer invoiceXmlJacksonSerializer,
                                            InvoiceDeserializer invoiceCsvJacksonDeserializer) {
        this.invoiceXmlJacksonSerializer = invoiceXmlJacksonSerializer;
        this.invoiceCsvJacksonDeserializer = invoiceCsvJacksonDeserializer;
    }

    @BeforeEach
    public void setup() {
        FileUtils.deleteFilesInDir(ACTUAL_RESULTS);
        this.invoiceXmlJacksonSerializer.setWritePath(ACTUAL_RESULTS);
    }

    @Test
    public void testXml() throws IOException {
        this.invoiceCsvJacksonDeserializer.deserialize(this.invoiceXmlJacksonSerializer::serialize);
        FileUtils.assertFilesEqual(ACTUAL_RESULTS, ".xml");
    }
}
