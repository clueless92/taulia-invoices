package bg.tilchev.tauliainvoice.integration;

import bg.tilchev.tauliainvoice.config.TestBeans;
import bg.tilchev.tauliainvoice.service.InvoiceDeserializer;
import bg.tilchev.tauliainvoice.service.InvoiceSerializer;
import bg.tilchev.tauliainvoice.util.FileUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.io.File;
import java.io.IOException;

/**
 * Created on 2022-10-20.
 */
@SpringBootTest(classes = TestBeans.class)
@ActiveProfiles("test")
public class OpenCsvInvoicesTest {

    public static final File ACTUAL_RESULTS = new File("build/src/test/resources/output/"
            + OpenCsvInvoicesTest.class.getSimpleName());
    private final InvoiceSerializer invoiceCsvOpenSerializer;
    private final InvoiceDeserializer invoiceCsvOpenDeserializer;

    @Autowired
    public OpenCsvInvoicesTest(InvoiceSerializer invoiceCsvOpenSerializer,
                               InvoiceDeserializer invoiceCsvOpenDeserializer) {
        this.invoiceCsvOpenSerializer = invoiceCsvOpenSerializer;
        this.invoiceCsvOpenDeserializer = invoiceCsvOpenDeserializer;
    }

    @BeforeEach
    public void setup() {
        FileUtils.deleteFilesInDir(ACTUAL_RESULTS);
        this.invoiceCsvOpenSerializer.setWritePath(ACTUAL_RESULTS);
    }

    @Test
    public void testCsv() throws IOException {
        this.invoiceCsvOpenDeserializer.deserialize(this.invoiceCsvOpenSerializer::serialize);
        FileUtils.assertFilesEqual(ACTUAL_RESULTS, ".csv");
    }
}
