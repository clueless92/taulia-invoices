package bg.tilchev.tauliainvoice.util;

import org.junit.jupiter.api.Assertions;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Objects;

/**
 * Created on 2022-10-24.
 */
public class FileUtils {
    private static final File EXPECTED_RESULTS = new File("src/test/resources/output");

    public static void deleteFilesInDir(File file) {
        if (file.isDirectory()) {
            Arrays.stream(Objects.requireNonNull(file.listFiles()))
                    .forEach(File::delete);
        }
    }

    public static void assertFilesEqual(File actualDir, String extension) throws IOException {
        File[] actualFiles = actualDir.listFiles(f -> f.getName().endsWith(extension));
        File[] expectedFiles = EXPECTED_RESULTS.listFiles(f -> f.getName().endsWith(extension));
        if (actualFiles == null || expectedFiles == null || actualFiles.length != expectedFiles.length) {
            Assertions.fail();
            return;
        }
        for (int i = 0; i < expectedFiles.length; i++) {
            File actual = actualFiles[i];
            File expected = expectedFiles[i];
            assertFilesByLine(actual, expected);
        }
    }

    private static void assertFilesByLine(File file1, File file2) throws IOException {
        try (var br1 = Files.newBufferedReader(file1.toPath());
             var br2 = Files.newBufferedReader(file2.toPath())) {

            long lineNumber = 1;
            String line1;
            String line2;
            while ((line1 = br1.readLine()) != null) {
                line2 = br2.readLine();
                if (line1.trim().startsWith("<invoice_image>")) {
                    // TODO assert line better
                    // We don't want to compare these line fully because of differences in each test actual results path
                    Assertions.assertTrue(line1.trim().startsWith("<invoice_image>"));
                    Assertions.assertTrue(line2.trim().startsWith("<invoice_image>"));
                    Assertions.assertTrue(line1.trim().endsWith("</invoice_image>"));
                    Assertions.assertTrue(line2.trim().endsWith("</invoice_image>"));
                } else {
                    Assertions.assertEquals(line1, line2, "Mismatch on line " + lineNumber);
                }
                lineNumber++;
            }
            if (br2.readLine() != null) {
                Assertions.fail("Mismatch on line " + lineNumber);
            }
        }
    }
}
